#!/usr/bin/env bash

db_host="localhost"
db_name="awarie"
db_user="postgres"
db_pass="12345"

export PGPASSWORD=$db_pass
psql -h $db_host -d $db_name -U $db_user -a -w -f init.sql