<?php

const root = "/";
const db_host = "localhost";
const db_name = "awarie";
const db_user = "postgres";
const db_pass = "12345";

session_start();
include_once "kontrolery.php";
include_once "model.php";

/**
 * Funkcja zwraca zbudowany adres
 * @param null $akcja
 * @param bool $powrot
 * informacja o tym, czy po wykonaniu akcji ma nastąpić powrót do wcześniejszego adresu
 * @return string
 */
function adres($akcja = null, $powrot = false){
    $adr = root;
    if ($akcja)
        $adr .= "?akcja=$akcja";
    else if (isset($_GET['przekieruj-do']))
        $adr .= "?akcja=" . $_GET['przekieruj-do'];
    if ($powrot && isset($_GET['akcja']))
        $adr .= "&przekieruj-do=" . $_GET['akcja'];
    return $adr;
}

/**
 * Funkcja przekierowuje na inny adres
 * @param null $akcja
 */
function przekieruj($akcja = null){
    header("Location: " . adres($akcja));
    die();
}

/**
 * Wytwarza stronę html na podstawie akcji, szablonow i parametrow
 * @param $akcja
 * @param $parametry
 */
function widok($akcja, $parametry){
    extract($parametry);
    $template = "szablony/" . $akcja . ".phtml";
    include_once "szablony/szablon.phtml";
    unset($_SESSION['blad']);
}

// główne dzialania skryptu - na podstawie parametrów,
// ktore zwrocil kontoler() nalezy stworzyc odpowiednia
// strone html

$akcja = isset($_GET['akcja']) ? $_GET['akcja'] : null;
if (!isset($_SESSION['uzytkownik_id']) && $akcja != "logowanie_wykonaj")
    $akcja = "logowanie";
else if (!isset($_GET['akcja']))
    $akcja = "informacje";
else if (!function_exists($akcja . "_kontroler"))
    $akcja = "blad_404";

$kontroler = $akcja . "_kontroler";
$parametry = $kontroler();
widok($akcja, $parametry);

