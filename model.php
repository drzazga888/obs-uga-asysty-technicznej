<?php

/**
 * Funkcja wywołuje funkcję składowaną po stronie bazy danych i zwraca wynik jej działania
 * @param $function
 * nazwa funkcji
 * @param $params
 * parametry dla funcji jak tablica asocjacyjna
 * @return array
 * wynik działania funkcji
 */
function wywolaj_funkcje_skladowana($function, $params){
    $query = "SELECT * FROM $function(";
    foreach ($params as $nazwa => &$param)
        $query .= ":$nazwa, ";
    if (!empty($params))
        $query = substr($query, 0, -2);
    $query .= ")";
    return wykonaj_zapytanie($query, $params);
}

/***
 * Funkcja pobiera widok bądź tabelę z bazy danych
 * @param $widok
 * nazwa widoku / tabeli
 * @param null $params
 * parametry użyte do klauzuli WHERE
 * @param null $warunek
 * warunek, czyli to co się po klauzuli WHERE
 * @return array
 * tabela jako tablica tablic asosjacyjnych
 */
function wywolaj_widok($widok, $params = null, $warunek = null){
    $query = "SELECT * FROM $widok";
    if ($warunek)
        $query .= " WHERE $warunek";
    return wykonaj_zapytanie($query, $params);
}

/**
 * Funkcja wykonuje odpowiednie zapytanie sql do bazy i zwraca ich wynik
 * @param $zapytanie
 * @param $params
 * @return array
 */
function wykonaj_zapytanie($zapytanie, $params){
    //try {
    //echo "<pre>$zapytanie\n";
    //var_dump($params);
    //echo '</pre>';
    $db = new PDO("pgsql:host=" . db_host . ";dbname=" . db_name . ";user=" . db_user . ";password=" . db_pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $db->prepare($zapytanie);
    if ($params) {
        foreach ($params as $nazwa => &$param)
            $stmt->bindValue(":" . $nazwa, $param);
    }
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
    //} catch (PDOException $e) {
    //    print "Błąd bazy danych!: " . $e->getMessage() . "<br/>";
    //    die();
    //}
}

/**
 * Funkcja pomocnicza do sprawdzania dostępu
 * @param $typ
 * nazwa typu dostępu do sprawdzenia
 * @return bool
 * czy dostęp jest dozwolony
 */
function spradz_dostep($typ){
    $dostep = wywolaj_widok("widok_dostep", array(
        "id" => $_SESSION['uzytkownik_id']
    ), "id = :id");
    if (!$dostep[0][$typ]){
        $_SESSION['blad'] = "Brak dostępu";
        przekieruj("wyloguj");
        return false;
    }
    return true;
}
