-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.2-alpha1
-- PostgreSQL version: 9.4
-- Project Site: pgmodeler.com.br
-- Model Author: ---

SET check_function_bodies = false;
-- ddl-end --


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: awarie | type: DATABASE --
-- DROP DATABASE IF EXISTS awarie;
-- CREATE DATABASE awarie
-- ;
-- -- ddl-end --
-- 

-- object: public.telefon | type: DOMAIN --
DROP DOMAIN IF EXISTS public.telefon CASCADE;
CREATE DOMAIN public.telefon AS char(9)
	CONSTRAINT telefon_poprawny CHECK (VALUE SIMILAR TO '[0-9]{9}');
-- ddl-end --

-- object: public.email | type: DOMAIN --
DROP DOMAIN IF EXISTS public.email CASCADE;
CREATE DOMAIN public.email AS varchar(30)
	CONSTRAINT email_poprawny CHECK (VALUE <> '' AND VALUE LIKE '_%@__%.__%');
-- ddl-end --

-- object: public.pracownik | type: TABLE --
DROP TABLE IF EXISTS public.pracownik CASCADE;
CREATE TABLE public.pracownik(
	id serial NOT NULL,
	imie varchar(20) NOT NULL,
	nazwisko varchar(20) NOT NULL,
	login varchar(20) NOT NULL,
	haslo char(40) NOT NULL,
	urlop boolean NOT NULL,
	telefon public.telefon,
	email public.email,
	id_dzial integer NOT NULL,
	id_typ_uzytkownika integer NOT NULL,
	CONSTRAINT pracownik_pk PRIMARY KEY (id),
	CONSTRAINT pracownik_jakikolwiek_kontakt CHECK ((telefon IS NOT NULL) OR (email IS NOT NULL)),
	CONSTRAINT pracownik_telefon_uq UNIQUE (telefon),
	CONSTRAINT pracownik_emial_uq UNIQUE (email)

);
-- ddl-end --

-- object: public.awaria | type: TABLE --
DROP TABLE IF EXISTS public.awaria CASCADE;
CREATE TABLE public.awaria(
	id serial NOT NULL,
	temat text NOT NULL,
	opis text NOT NULL,
	data_zgloszenia timestamp NOT NULL DEFAULT current_timestamp,
	id_pracownik integer NOT NULL,
	id_typ_awarii integer,
	id_priorytet_awarii integer NOT NULL,
	id_stan_awarii integer NOT NULL,
	CONSTRAINT awaria_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.typ_awarii | type: TABLE --
DROP TABLE IF EXISTS public.typ_awarii CASCADE;
CREATE TABLE public.typ_awarii(
	id serial NOT NULL,
	nazwa text NOT NULL,
	rodzic integer,
	CONSTRAINT typ_awarii_pk PRIMARY KEY (id),
	CONSTRAINT typ_awarii_nazwa_uq UNIQUE (nazwa)

);
-- ddl-end --

-- object: public.awaria_specjalista | type: TABLE --
DROP TABLE IF EXISTS public.awaria_specjalista CASCADE;
CREATE TABLE public.awaria_specjalista(
	id_awaria integer NOT NULL,
	id_pracownik_specjalista integer NOT NULL,
	CONSTRAINT awaria_specjalista_pk PRIMARY KEY (id_awaria)

);
-- ddl-end --

-- object: public.typ_uzytkownika | type: TABLE --
DROP TABLE IF EXISTS public.typ_uzytkownika CASCADE;
CREATE TABLE public.typ_uzytkownika(
	id serial NOT NULL,
	nazwa varchar(20) NOT NULL,
	dostep_do_awarii boolean NOT NULL,
	dostep_do_przydzielonych_awarii boolean NOT NULL,
	dostep_do_typow_awarii boolean NOT NULL,
	dostep_do_specjalistow boolean NOT NULL,
	dostep_do_swoich_awarii boolean NOT NULL,
	CONSTRAINT typ_uzytkownika_pk PRIMARY KEY (id),
	CONSTRAINT typ_uzytkownika_uq UNIQUE (nazwa)

);
-- ddl-end --

-- object: public.awaria_przebieg | type: TABLE --
DROP TABLE IF EXISTS public.awaria_przebieg CASCADE;
CREATE TABLE public.awaria_przebieg(
	id serial NOT NULL,
	data timestamp NOT NULL DEFAULT current_timestamp,
	id_awaria integer NOT NULL,
	id_stan_awarii integer NOT NULL,
	CONSTRAINT awaria_przebieg_pk PRIMARY KEY (id)

);
-- ddl-end --

-- object: public.specjalista | type: TABLE --
DROP TABLE IF EXISTS public.specjalista CASCADE;
CREATE TABLE public.specjalista(
	ilosc_napraw integer NOT NULL DEFAULT 0,
	ilosc_pomyslnych_napraw integer NOT NULL DEFAULT 0,
	id_pracownik integer NOT NULL,
	id_typ_awarii integer,
	CONSTRAINT specjalista_pk PRIMARY KEY (id_pracownik)

);
-- ddl-end --

-- object: public.awaria_przebieg_specjalista | type: TABLE --
DROP TABLE IF EXISTS public.awaria_przebieg_specjalista CASCADE;
CREATE TABLE public.awaria_przebieg_specjalista(
	id_awaria_przebieg integer NOT NULL,
	id_pracownik_specjalista integer NOT NULL,
	CONSTRAINT awaria_przebieg_specjalista_pk PRIMARY KEY (id_awaria_przebieg)

);
-- ddl-end --

-- object: public.awaria_przebieg_powod | type: TABLE --
DROP TABLE IF EXISTS public.awaria_przebieg_powod CASCADE;
CREATE TABLE public.awaria_przebieg_powod(
	id_awaria_przebieg integer NOT NULL,
	powod text NOT NULL,
	sugestie text,
	CONSTRAINT awaria_przebieg_powod_pk PRIMARY KEY (id_awaria_przebieg)

);
-- ddl-end --

-- object: public.dzial | type: TABLE --
DROP TABLE IF EXISTS public.dzial CASCADE;
CREATE TABLE public.dzial(
	id serial NOT NULL,
	nazwa varchar(30) NOT NULL,
	id_miejsce_pracy integer NOT NULL,
	CONSTRAINT dzial_pk PRIMARY KEY (id),
	CONSTRAINT dzial_nazwa_uq UNIQUE (nazwa)

);
-- ddl-end --

-- object: dzial_fk | type: CONSTRAINT --
ALTER TABLE public.pracownik DROP CONSTRAINT IF EXISTS dzial_fk CASCADE;
ALTER TABLE public.pracownik ADD CONSTRAINT dzial_fk FOREIGN KEY (id_dzial)
REFERENCES public.dzial (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: pracownik_fk | type: CONSTRAINT --
ALTER TABLE public.awaria DROP CONSTRAINT IF EXISTS pracownik_fk CASCADE;
ALTER TABLE public.awaria ADD CONSTRAINT pracownik_fk FOREIGN KEY (id_pracownik)
REFERENCES public.pracownik (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: typ_awarii_fk | type: CONSTRAINT --
ALTER TABLE public.awaria DROP CONSTRAINT IF EXISTS typ_awarii_fk CASCADE;
ALTER TABLE public.awaria ADD CONSTRAINT typ_awarii_fk FOREIGN KEY (id_typ_awarii)
REFERENCES public.typ_awarii (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_specjalista DROP CONSTRAINT IF EXISTS awaria_fk CASCADE;
ALTER TABLE public.awaria_specjalista ADD CONSTRAINT awaria_fk FOREIGN KEY (id_awaria)
REFERENCES public.awaria (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_specjalista_uq | type: CONSTRAINT --
ALTER TABLE public.awaria_specjalista DROP CONSTRAINT IF EXISTS awaria_specjalista_uq CASCADE;
ALTER TABLE public.awaria_specjalista ADD CONSTRAINT awaria_specjalista_uq UNIQUE (id_awaria);
-- ddl-end --

-- object: awaria_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg DROP CONSTRAINT IF EXISTS awaria_fk CASCADE;
ALTER TABLE public.awaria_przebieg ADD CONSTRAINT awaria_fk FOREIGN KEY (id_awaria)
REFERENCES public.awaria (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_przebieg_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg_specjalista DROP CONSTRAINT IF EXISTS awaria_przebieg_fk CASCADE;
ALTER TABLE public.awaria_przebieg_specjalista ADD CONSTRAINT awaria_przebieg_fk FOREIGN KEY (id_awaria_przebieg)
REFERENCES public.awaria_przebieg (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_przebieg_specjalista_uq | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg_specjalista DROP CONSTRAINT IF EXISTS awaria_przebieg_specjalista_uq CASCADE;
ALTER TABLE public.awaria_przebieg_specjalista ADD CONSTRAINT awaria_przebieg_specjalista_uq UNIQUE (id_awaria_przebieg);
-- ddl-end --

-- object: awaria_przebieg_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg_powod DROP CONSTRAINT IF EXISTS awaria_przebieg_fk CASCADE;
ALTER TABLE public.awaria_przebieg_powod ADD CONSTRAINT awaria_przebieg_fk FOREIGN KEY (id_awaria_przebieg)
REFERENCES public.awaria_przebieg (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_przebieg_powod_uq | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg_powod DROP CONSTRAINT IF EXISTS awaria_przebieg_powod_uq CASCADE;
ALTER TABLE public.awaria_przebieg_powod ADD CONSTRAINT awaria_przebieg_powod_uq UNIQUE (id_awaria_przebieg);
-- ddl-end --

-- object: public.awaria_oczekujaca | type: TABLE --
DROP TABLE IF EXISTS public.awaria_oczekujaca CASCADE;
CREATE TABLE public.awaria_oczekujaca(
	id_awaria integer NOT NULL,
	id_pracownik_specjalista integer NOT NULL,
	CONSTRAINT awaria_oczekujaca_pk PRIMARY KEY (id_awaria)

);
-- ddl-end --

-- object: awaria_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_oczekujaca DROP CONSTRAINT IF EXISTS awaria_fk CASCADE;
ALTER TABLE public.awaria_oczekujaca ADD CONSTRAINT awaria_fk FOREIGN KEY (id_awaria)
REFERENCES public.awaria (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_oczekujaca_uq | type: CONSTRAINT --
ALTER TABLE public.awaria_oczekujaca DROP CONSTRAINT IF EXISTS awaria_oczekujaca_uq CASCADE;
ALTER TABLE public.awaria_oczekujaca ADD CONSTRAINT awaria_oczekujaca_uq UNIQUE (id_awaria);
-- ddl-end --

-- object: public.miejsce_pracy | type: TABLE --
DROP TABLE IF EXISTS public.miejsce_pracy CASCADE;
CREATE TABLE public.miejsce_pracy(
	id serial NOT NULL,
	ulica varchar(30) NOT NULL,
	nr_domu varchar(10) NOT NULL,
	kod_pocztowy varchar(10) NOT NULL,
	miejscowosc varchar(30) NOT NULL,
	CONSTRAINT miejsce_pracy_pk PRIMARY KEY (id),
	CONSTRAINT miejsce_pracy_uq UNIQUE (ulica,nr_domu,kod_pocztowy,miejscowosc)

);
-- ddl-end --

-- object: pracownik_fk | type: CONSTRAINT --
ALTER TABLE public.specjalista DROP CONSTRAINT IF EXISTS pracownik_fk CASCADE;
ALTER TABLE public.specjalista ADD CONSTRAINT pracownik_fk FOREIGN KEY (id_pracownik)
REFERENCES public.pracownik (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: specjalista_uq | type: CONSTRAINT --
ALTER TABLE public.specjalista DROP CONSTRAINT IF EXISTS specjalista_uq CASCADE;
ALTER TABLE public.specjalista ADD CONSTRAINT specjalista_uq UNIQUE (id_pracownik);
-- ddl-end --

-- object: typ_awarii_fk | type: CONSTRAINT --
ALTER TABLE public.specjalista DROP CONSTRAINT IF EXISTS typ_awarii_fk CASCADE;
ALTER TABLE public.specjalista ADD CONSTRAINT typ_awarii_fk FOREIGN KEY (id_typ_awarii)
REFERENCES public.typ_awarii (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: specjalista_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_specjalista DROP CONSTRAINT IF EXISTS specjalista_fk CASCADE;
ALTER TABLE public.awaria_specjalista ADD CONSTRAINT specjalista_fk FOREIGN KEY (id_pracownik_specjalista)
REFERENCES public.specjalista (id_pracownik) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: awaria_specjalista_uq1 | type: CONSTRAINT --
ALTER TABLE public.awaria_specjalista DROP CONSTRAINT IF EXISTS awaria_specjalista_uq1 CASCADE;
ALTER TABLE public.awaria_specjalista ADD CONSTRAINT awaria_specjalista_uq1 UNIQUE (id_pracownik_specjalista);
-- ddl-end --

-- object: specjalista_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_oczekujaca DROP CONSTRAINT IF EXISTS specjalista_fk CASCADE;
ALTER TABLE public.awaria_oczekujaca ADD CONSTRAINT specjalista_fk FOREIGN KEY (id_pracownik_specjalista)
REFERENCES public.specjalista (id_pracownik) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: specjalista_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg_specjalista DROP CONSTRAINT IF EXISTS specjalista_fk CASCADE;
ALTER TABLE public.awaria_przebieg_specjalista ADD CONSTRAINT specjalista_fk FOREIGN KEY (id_pracownik_specjalista)
REFERENCES public.specjalista (id_pracownik) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: miejsce_pracy_fk | type: CONSTRAINT --
ALTER TABLE public.dzial DROP CONSTRAINT IF EXISTS miejsce_pracy_fk CASCADE;
ALTER TABLE public.dzial ADD CONSTRAINT miejsce_pracy_fk FOREIGN KEY (id_miejsce_pracy)
REFERENCES public.miejsce_pracy (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.priorytet_awarii | type: TABLE --
DROP TABLE IF EXISTS public.priorytet_awarii CASCADE;
CREATE TABLE public.priorytet_awarii(
	id serial NOT NULL,
	nazwa varchar(20) NOT NULL,
	CONSTRAINT priorytet_awarii_pk PRIMARY KEY (id),
	CONSTRAINT priorytet_awarii_uq UNIQUE (nazwa)

);
-- ddl-end --

-- object: priorytet_awarii_fk | type: CONSTRAINT --
ALTER TABLE public.awaria DROP CONSTRAINT IF EXISTS priorytet_awarii_fk CASCADE;
ALTER TABLE public.awaria ADD CONSTRAINT priorytet_awarii_fk FOREIGN KEY (id_priorytet_awarii)
REFERENCES public.priorytet_awarii (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: typ_uzytkownika_fk | type: CONSTRAINT --
ALTER TABLE public.pracownik DROP CONSTRAINT IF EXISTS typ_uzytkownika_fk CASCADE;
ALTER TABLE public.pracownik ADD CONSTRAINT typ_uzytkownika_fk FOREIGN KEY (id_typ_uzytkownika)
REFERENCES public.typ_uzytkownika (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.stan_awarii | type: TABLE --
DROP TABLE IF EXISTS public.stan_awarii CASCADE;
CREATE TABLE public.stan_awarii(
	id serial NOT NULL,
	nazwa varchar(20) NOT NULL,
	CONSTRAINT stan_awarii_pk PRIMARY KEY (id),
	CONSTRAINT stan_awarii_uq UNIQUE (nazwa)

);
-- ddl-end --

-- object: stan_awarii_fk | type: CONSTRAINT --
ALTER TABLE public.awaria DROP CONSTRAINT IF EXISTS stan_awarii_fk CASCADE;
ALTER TABLE public.awaria ADD CONSTRAINT stan_awarii_fk FOREIGN KEY (id_stan_awarii)
REFERENCES public.stan_awarii (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: stan_awarii_fk | type: CONSTRAINT --
ALTER TABLE public.awaria_przebieg DROP CONSTRAINT IF EXISTS stan_awarii_fk CASCADE;
ALTER TABLE public.awaria_przebieg ADD CONSTRAINT stan_awarii_fk FOREIGN KEY (id_stan_awarii)
REFERENCES public.stan_awarii (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.trigger_awaria_przed_wstawieniem | type: FUNCTION --
DROP FUNCTION IF EXISTS public.trigger_awaria_przed_wstawieniem() CASCADE;
CREATE FUNCTION public.trigger_awaria_przed_wstawieniem ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN
SELECT id INTO new.id_stan_awarii FROM stan_awarii WHERE nazwa = 'Nie przydzielono';
RETURN new;
END;
$$;
-- ddl-end --

-- object: awaria_przed_wstawieniem | type: TRIGGER --
DROP TRIGGER IF EXISTS awaria_przed_wstawieniem ON public.awaria CASCADE;
CREATE TRIGGER awaria_przed_wstawieniem
	BEFORE INSERT 
	ON public.awaria
	FOR EACH ROW
	EXECUTE PROCEDURE public.trigger_awaria_przed_wstawieniem();
-- ddl-end --

-- object: public.trigger_dodaj_nowego_specjaliste | type: FUNCTION --
DROP FUNCTION IF EXISTS public.trigger_dodaj_nowego_specjaliste() CASCADE;
CREATE FUNCTION public.trigger_dodaj_nowego_specjaliste ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	id_jako_specjalista integer;
BEGIN
SELECT id INTO id_jako_specjalista FROM typ_uzytkownika
WHERE nazwa = 'Specjalista';

IF new.id_typ_uzytkownika = id_jako_specjalista THEN
	INSERT INTO specjalista (
		id_pracownik
	) VALUES
	(new.id);
END IF;

RETURN NULL;
END;
$$;
-- ddl-end --

-- object: nowy_pracownik_dodany | type: TRIGGER --
DROP TRIGGER IF EXISTS nowy_pracownik_dodany ON public.pracownik CASCADE;
CREATE TRIGGER nowy_pracownik_dodany
	AFTER INSERT 
	ON public.pracownik
	FOR EACH ROW
	EXECUTE PROCEDURE public.trigger_dodaj_nowego_specjaliste();
-- ddl-end --

-- object: public.trigger_awaria_zapisz_nowy_przebieg | type: FUNCTION --
DROP FUNCTION IF EXISTS public.trigger_awaria_zapisz_nowy_przebieg() CASCADE;
CREATE FUNCTION public.trigger_awaria_zapisz_nowy_przebieg ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	stan_nazwa text;
BEGIN

SELECT nazwa INTO stan_nazwa FROM stan_awarii
WHERE id = new.id_stan_awarii;

	INSERT INTO awaria_przebieg (
		id_awaria,
		id_stan_awarii
	) VALUES
	(
		new.id,
		new.id_stan_awarii
	);

RETURN NULL;
END;
$$;
-- ddl-end --

-- object: awaria_zmiana_stanu | type: TRIGGER --
DROP TRIGGER IF EXISTS awaria_zmiana_stanu ON public.awaria CASCADE;
CREATE TRIGGER awaria_zmiana_stanu
	AFTER INSERT 
	ON public.awaria
	FOR EACH ROW
	EXECUTE PROCEDURE public.trigger_awaria_zapisz_nowy_przebieg();
-- ddl-end --

-- object: public.trigger_usun_przestarzale_awarie | type: FUNCTION --
DROP FUNCTION IF EXISTS public.trigger_usun_przestarzale_awarie() CASCADE;
CREATE FUNCTION public.trigger_usun_przestarzale_awarie ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

DELETE FROM awaria WHERE id NOT IN (
	SELECT id FROM awaria
	WHERE awaria.id_pracownik = new.id_pracownik
	AND awaria.id_stan_awarii IN (
		SELECT id FROM stan_awarii
		WHERE nazwa IN ('Zarzucono', 'Zaakceptowano')
	)
	ORDER BY data_zgloszenia DESC, id DESC LIMIT 3
)
AND awaria.id_pracownik = new.id_pracownik
AND awaria.id_stan_awarii IN (
	SELECT id FROM stan_awarii
	WHERE nazwa IN ('Zarzucono', 'Zaakceptowano')
);
RETURN NULL;

END;
$$;
-- ddl-end --

-- object: awaria_czyszczenie | type: TRIGGER --
DROP TRIGGER IF EXISTS awaria_czyszczenie ON public.awaria CASCADE;
CREATE TRIGGER awaria_czyszczenie
	AFTER INSERT OR UPDATE
	ON public.awaria
	FOR EACH ROW
	EXECUTE PROCEDURE public.trigger_usun_przestarzale_awarie();
-- ddl-end --

-- object: indeks_typ_uzytkownika_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_typ_uzytkownika_id CASCADE;
CREATE INDEX indeks_typ_uzytkownika_id ON public.typ_uzytkownika
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_typ_uzytkownika_nazwa | type: INDEX --
DROP INDEX IF EXISTS public.indeks_typ_uzytkownika_nazwa CASCADE;
CREATE INDEX indeks_typ_uzytkownika_nazwa ON public.typ_uzytkownika
	USING btree
	(
	  nazwa
	);
-- ddl-end --

-- object: indeks_pracownik_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_pracownik_id CASCADE;
CREATE INDEX indeks_pracownik_id ON public.pracownik
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_pracownik_dzial | type: INDEX --
DROP INDEX IF EXISTS public.indeks_pracownik_dzial CASCADE;
CREATE INDEX indeks_pracownik_dzial ON public.pracownik
	USING btree
	(
	  id_dzial
	);
-- ddl-end --

-- object: indeks_pracownik_typ_uzytkownika | type: INDEX --
DROP INDEX IF EXISTS public.indeks_pracownik_typ_uzytkownika CASCADE;
CREATE INDEX indeks_pracownik_typ_uzytkownika ON public.pracownik
	USING btree
	(
	  id_typ_uzytkownika
	);
-- ddl-end --

-- object: indeks_dzial_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_dzial_id CASCADE;
CREATE INDEX indeks_dzial_id ON public.dzial
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_dzial_miejsce_pracy | type: INDEX --
DROP INDEX IF EXISTS public.indeks_dzial_miejsce_pracy CASCADE;
CREATE INDEX indeks_dzial_miejsce_pracy ON public.dzial
	USING btree
	(
	  id_miejsce_pracy
	);
-- ddl-end --

-- object: indeks_miejsce_pracy_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_miejsce_pracy_id CASCADE;
CREATE INDEX indeks_miejsce_pracy_id ON public.miejsce_pracy
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_specjalista_pracownik | type: INDEX --
DROP INDEX IF EXISTS public.indeks_specjalista_pracownik CASCADE;
CREATE INDEX indeks_specjalista_pracownik ON public.specjalista
	USING btree
	(
	  id_pracownik
	);
-- ddl-end --

-- object: indeks_specjalista_typ_awarii | type: INDEX --
DROP INDEX IF EXISTS public.indeks_specjalista_typ_awarii CASCADE;
CREATE INDEX indeks_specjalista_typ_awarii ON public.specjalista
	USING btree
	(
	  id_typ_awarii
	);
-- ddl-end --

-- object: indeks_typ_awarii_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_typ_awarii_id CASCADE;
CREATE INDEX indeks_typ_awarii_id ON public.typ_awarii
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_typ_awarii_nazwa | type: INDEX --
DROP INDEX IF EXISTS public.indeks_typ_awarii_nazwa CASCADE;
CREATE INDEX indeks_typ_awarii_nazwa ON public.typ_awarii
	USING btree
	(
	  nazwa
	);
-- ddl-end --

-- object: indeks_typ_awarii_rodzic | type: INDEX --
DROP INDEX IF EXISTS public.indeks_typ_awarii_rodzic CASCADE;
CREATE INDEX indeks_typ_awarii_rodzic ON public.typ_awarii
	USING btree
	(
	  rodzic
	);
-- ddl-end --

-- object: indeks_awaria_specjalista_awaria | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_specjalista_awaria CASCADE;
CREATE INDEX indeks_awaria_specjalista_awaria ON public.awaria_specjalista
	USING btree
	(
	  id_awaria
	);
-- ddl-end --

-- object: indeks_awaria_specjalista_specjalista | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_specjalista_specjalista CASCADE;
CREATE INDEX indeks_awaria_specjalista_specjalista ON public.awaria_specjalista
	USING btree
	(
	  id_pracownik_specjalista
	);
-- ddl-end --

-- object: indeks_awaria_oczekujaca_awaria | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_oczekujaca_awaria CASCADE;
CREATE INDEX indeks_awaria_oczekujaca_awaria ON public.awaria_oczekujaca
	USING btree
	(
	  id_awaria
	);
-- ddl-end --

-- object: indeks_awaria_oczekujaca_specjalista | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_oczekujaca_specjalista CASCADE;
CREATE INDEX indeks_awaria_oczekujaca_specjalista ON public.awaria_oczekujaca
	USING btree
	(
	  id_pracownik_specjalista
	);
-- ddl-end --

-- object: indeks_awaria_przebieg_specjalista_awaria | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_przebieg_specjalista_awaria CASCADE;
CREATE INDEX indeks_awaria_przebieg_specjalista_awaria ON public.awaria_przebieg_specjalista
	USING btree
	(
	  id_awaria_przebieg
	);
-- ddl-end --

-- object: indeks_awaria_przebieg_specjalista_specjalista | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_przebieg_specjalista_specjalista CASCADE;
CREATE INDEX indeks_awaria_przebieg_specjalista_specjalista ON public.awaria_przebieg_specjalista
	USING btree
	(
	  id_pracownik_specjalista
	);
-- ddl-end --

-- object: indeks_awaria_przebieg_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_przebieg_id CASCADE;
CREATE INDEX indeks_awaria_przebieg_id ON public.awaria_przebieg
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_awaria_przebieg_awaria | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_przebieg_awaria CASCADE;
CREATE INDEX indeks_awaria_przebieg_awaria ON public.awaria_przebieg
	USING btree
	(
	  id_awaria
	);
-- ddl-end --

-- object: indeks_awaria_przebieg_stan | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_przebieg_stan CASCADE;
CREATE INDEX indeks_awaria_przebieg_stan ON public.awaria_przebieg
	USING btree
	(
	  id_stan_awarii
	);
-- ddl-end --

-- object: indeks_awaria_przebieg_powod | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_przebieg_powod CASCADE;
CREATE INDEX indeks_awaria_przebieg_powod ON public.awaria_przebieg_powod
	USING btree
	(
	  id_awaria_przebieg
	);
-- ddl-end --

-- object: indeks_stan_awarii_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_stan_awarii_id CASCADE;
CREATE INDEX indeks_stan_awarii_id ON public.stan_awarii
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_stan_awarii_nazwa | type: INDEX --
DROP INDEX IF EXISTS public.indeks_stan_awarii_nazwa CASCADE;
CREATE INDEX indeks_stan_awarii_nazwa ON public.stan_awarii
	USING btree
	(
	  nazwa
	);
-- ddl-end --

-- object: indeks_priorytet_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_priorytet_id CASCADE;
CREATE INDEX indeks_priorytet_id ON public.priorytet_awarii
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_priorytet_nazwa | type: INDEX --
DROP INDEX IF EXISTS public.indeks_priorytet_nazwa CASCADE;
CREATE INDEX indeks_priorytet_nazwa ON public.priorytet_awarii
	USING btree
	(
	  nazwa
	);
-- ddl-end --

-- object: indeks_awaria_id | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_id CASCADE;
CREATE INDEX indeks_awaria_id ON public.awaria
	USING btree
	(
	  id
	);
-- ddl-end --

-- object: indeks_awaria_pracownik | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_pracownik CASCADE;
CREATE INDEX indeks_awaria_pracownik ON public.awaria
	USING btree
	(
	  id_pracownik
	);
-- ddl-end --

-- object: indeks_awaria_typ_awarii | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_typ_awarii CASCADE;
CREATE INDEX indeks_awaria_typ_awarii ON public.awaria
	USING btree
	(
	  id_typ_awarii
	);
-- ddl-end --

-- object: indeks_awaria_priorytet | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_priorytet CASCADE;
CREATE INDEX indeks_awaria_priorytet ON public.awaria
	USING btree
	(
	  id_priorytet_awarii
	);
-- ddl-end --

-- object: indeks_awaria_stan | type: INDEX --
DROP INDEX IF EXISTS public.indeks_awaria_stan CASCADE;
CREATE INDEX indeks_awaria_stan ON public.awaria
	USING btree
	(
	  id_stan_awarii
	);
-- ddl-end --

-- object: public.zmien_telefon | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zmien_telefon(integer,text) CASCADE;
CREATE FUNCTION public.zmien_telefon ( _id_uzytkownik integer,  _telefon text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF length(_telefon) <> 9 THEN
	RETURN 'Numer telefonu musi mieć 9 cyfr';
ELSIF _telefon NOT SIMILAR TO '[0-9]{9}' THEN
	RETURN 'Numer telefonu musi składać się wyłącznie z cyfr';
ELSIF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Użytkownik nie istnieje';
ELSIF _telefon IN (SELECT telefon FROM pracownik) THEN
	RETURN 'Istnieje już taki numer telefonu';
ELSE
	UPDATE pracownik SET telefon = _telefon WHERE id = _id_uzytkownik;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.zmien_email | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zmien_email(integer,text) CASCADE;
CREATE FUNCTION public.zmien_email ( _id_uzytkownik integer,  _email text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF length(_email) > 30 THEN
	RETURN 'Długość adresu e-mail nie może być większa od 30';
ELSIF length(_email) < 4 THEN
	RETURN 'Długość adresu e-mail nie może być mniejsza niż 4';
ELSIF _email NOT LIKE '_%@__%.__%' THEN
	RETURN 'Format adresu e-mail jest niepoprawny';
ELSIF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Użytkownik nie istnieje';
ELSIF _email IN (SELECT email FROM pracownik) THEN
	RETURN 'Podany adres e-mail już istnieje';
ELSE
	UPDATE pracownik SET email = _email WHERE id = _id_uzytkownik;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.zmien_haslo | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zmien_haslo(integer,text,text) CASCADE;
CREATE FUNCTION public.zmien_haslo ( _id_uzytkownik integer,  _stare_haslo text,  _nowe_haslo text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF _stare_haslo <> (SELECT haslo FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Stare hasło nie zgadza się z istniejącym';
ELSIF length(_stare_haslo) <> 40 OR length(_nowe_haslo) <> 40 THEN
	RETURN 'Zmiana hasła zakończyła się niepowodzeniem';
ELSE
	UPDATE pracownik SET haslo = _nowe_haslo WHERE id = _id_uzytkownik;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.ustaw_urlop | type: FUNCTION --
DROP FUNCTION IF EXISTS public.ustaw_urlop(integer) CASCADE;
CREATE FUNCTION public.ustaw_urlop ( _id_uzytkownik integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF (SELECT urlop FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Nie mozna ustawić ustawionego już urlopu';
ELSE
	UPDATE pracownik SET urlop = TRUE WHERE id = _id_uzytkownik;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.zdejmij_urlop | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zdejmij_urlop(integer) CASCADE;
CREATE FUNCTION public.zdejmij_urlop ( _id_uzytkownik integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT urlop FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Nie mozna zdjąć urlopu, gdyż nie masz go aktywnego';
ELSE
	UPDATE pracownik SET urlop = FALSE WHERE id = _id_uzytkownik;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.zmien_preferowany_typ_awarii | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zmien_preferowany_typ_awarii(integer,integer) CASCADE;
CREATE FUNCTION public.zmien_preferowany_typ_awarii ( _id_uzytkownik integer,  _id integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM specjalista
WHERE id_pracownik = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_przydzielonych_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF _id IS NOT NULL AND NOT EXISTS (SELECT 1 FROM typ_awarii
WHERE id = _id) THEN
	RETURN 'Typ awarii nie istnieje';
ELSE
	UPDATE specjalista SET id_typ_awarii = _id
	WHERE id_pracownik = _id_uzytkownik;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.dodaj_awarie | type: FUNCTION --
DROP FUNCTION IF EXISTS public.dodaj_awarie(integer,integer,text,text,integer) CASCADE;
CREATE FUNCTION public.dodaj_awarie ( _id_uzytkownik integer,  _typ integer,  _temat text,  _opis text,  _priorytet integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_swoich_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF NOT EXISTS (SELECT 1 FROM priorytet_awarii
WHERE id = _priorytet) THEN
	RETURN 'Błędny priorytet awarii';
ELSIF _typ IS NOT NULL AND NOT EXISTS (SELECT 1 FROM typ_awarii
WHERE id = _typ) THEN
	RETURN 'Typ awarii nie istnieje';
ELSIF length(_temat) = 0 THEN
	RETURN 'Pole "temat" musi zostać wypełnione';
ELSIF length(_opis) = 0 THEN
	RETURN 'Pole "opis" musi zostać wypełnione';
ELSE
	INSERT INTO awaria
	(
		temat,
		opis,
		id_priorytet_awarii,
		id_pracownik,
		id_typ_awarii
	)
	VALUES
	(
		_temat,
		_opis,
		_priorytet,
		_id_uzytkownik,
		_typ
	);
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.awaria_przydziel | type: FUNCTION --
DROP FUNCTION IF EXISTS public.awaria_przydziel(integer,integer,integer) CASCADE;
CREATE FUNCTION public.awaria_przydziel ( _id_uzytkownik integer,  _id_awaria integer,  _id_specjalista integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF NOT EXISTS (SELECT 1 FROM awaria WHERE id = _id_awaria) THEN
	RETURN 'Awaria nie istnieje';
ELSIF EXISTS (
SELECT 1 FROM awaria_specjalista, awaria_oczekujaca
WHERE awaria_specjalista.id_awaria = _id_awaria
OR awaria_oczekujaca.id_awaria = _id_awaria
) THEN
	RETURN 'Awaria jest już przydzielona';
ELSIF NOT EXISTS (SELECT 1 FROM specjalista WHERE id_pracownik = _id_specjalista) THEN
	RETURN 'Specjalista nie istnieje';
ELSIF NOT EXISTS (SELECT 1 FROM widok_aktywny_specjalista WHERE id = _id_specjalista) THEN
	RETURN 'Specjalista nie jest dostępny';
ELSE
	INSERT INTO awaria_oczekujaca VALUES (_id_awaria, _id_specjalista);
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.trigger_przydzielono_specjaliste | type: FUNCTION --
DROP FUNCTION IF EXISTS public.trigger_przydzielono_specjaliste() CASCADE;
CREATE FUNCTION public.trigger_przydzielono_specjaliste ()
	RETURNS trigger
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	id_przebieg integer;
BEGIN

UPDATE awaria SET id_stan_awarii = stan_awarii.id
FROM stan_awarii
WHERE awaria.id = new.id_awaria
AND stan_awarii.nazwa = 'Przydzielono';

INSERT INTO awaria_przebieg (
	id_awaria,
	id_stan_awarii
) VALUES
(
	new.id_awaria,
	(SELECT id FROM stan_awarii WHERE nazwa = 'Przydzielono')
)
RETURNING id INTO id_przebieg;

INSERT INTO awaria_przebieg_specjalista VALUES (
	id_przebieg, new.id_pracownik_specjalista
);

RETURN NULL;

END;
$$;
-- ddl-end --

-- object: przydzielono_specjaliste | type: TRIGGER --
DROP TRIGGER IF EXISTS przydzielono_specjaliste ON public.awaria_oczekujaca CASCADE;
CREATE TRIGGER przydzielono_specjaliste
	AFTER INSERT 
	ON public.awaria_oczekujaca
	FOR EACH ROW
	EXECUTE PROCEDURE public.trigger_przydzielono_specjaliste();
-- ddl-end --

-- object: public.awaria_utrzymana | type: FUNCTION --
DROP FUNCTION IF EXISTS public.awaria_utrzymana(integer,integer,text,text) CASCADE;
CREATE FUNCTION public.awaria_utrzymana ( _id_uzytkownik integer,  _id integer,  _powod text,  _sugestie text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	id_przebieg integer;
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF length(_powod) = 0 THEN
	RETURN 'Pole powód musi być wypełnione';
ELSE
	DELETE FROM awaria_oczekujaca WHERE id_awaria = _id;
	DELETE FROM awaria_specjalista WHERE id_awaria = _id;
	IF (SELECT id FROM stan_awarii WHERE nazwa = 'W trakcie naprawy')
	= (SELECT id_stan_awarii FROM awaria WHERE id = _id) THEN
		UPDATE specjalista SET
		ilosc_napraw = ilosc_napraw + 1
		WHERE id_pracownik = _id_uzytkownik;
	END IF;
	UPDATE awaria SET id_stan_awarii = stan_awarii.id
	FROM stan_awarii
	WHERE stan_awarii.nazwa = 'Nie przydzielono'
	AND awaria.id = _id;
	INSERT INTO awaria_przebieg (
		id_awaria,
		id_stan_awarii
	) VALUES
	(
		_id,
		(SELECT id FROM stan_awarii WHERE nazwa = 'Nie przydzielono')
	)
	RETURNING id INTO id_przebieg;
	INSERT INTO awaria_przebieg_powod VALUES (
		id_przebieg, _powod, _sugestie
	);
	IF EXISTS (SELECT 1 FROM specjalista WHERE id_pracownik = _id_uzytkownik) THEN
		INSERT INTO awaria_przebieg_specjalista VALUES (
			id_przebieg, _id_uzytkownik
		);
	END IF;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.awaria_zarzuc | type: FUNCTION --
DROP FUNCTION IF EXISTS public.awaria_zarzuc(integer,integer,text,text) CASCADE;
CREATE FUNCTION public.awaria_zarzuc ( _id_uzytkownik integer,  _id integer,  _powod text,  _sugestie text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	id_przebieg integer;
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF length(_powod) = 0 THEN
	RETURN 'Pole powód musi być wypełnione';
ELSE
	UPDATE awaria SET id_stan_awarii = stan_awarii.id
	FROM stan_awarii WHERE stan_awarii.nazwa = 'Zarzucono'
	AND awaria.id = _id;
	INSERT INTO awaria_przebieg (
		id_awaria,
		id_stan_awarii
	) VALUES
	(
		_id,
		(SELECT id FROM stan_awarii WHERE nazwa = 'Zarzucono')
	)
	RETURNING id INTO id_przebieg;
	INSERT INTO awaria_przebieg_powod VALUES (
		id_przebieg, _powod, _sugestie
	);
	RETURN 'OK';

END IF;

END;
$$;
-- ddl-end --

-- object: public.awaria_przyjmij | type: FUNCTION --
DROP FUNCTION IF EXISTS public.awaria_przyjmij(integer,integer) CASCADE;
CREATE FUNCTION public.awaria_przyjmij ( _id_uzytkownik integer,  _id integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	id_przebieg integer;
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSE
	DELETE FROM awaria_oczekujaca WHERE id_awaria = _id;
	INSERT INTO awaria_specjalista VALUES (
		_id,
		_id_uzytkownik		
	);
	UPDATE awaria SET id_stan_awarii = stan_awarii.id
	FROM stan_awarii
	WHERE stan_awarii.nazwa = 'W trakcie naprawy'
	AND awaria.id = _id;
	INSERT INTO awaria_przebieg (
		id_awaria,
		id_stan_awarii
	) VALUES
	(
		_id,
		(SELECT id FROM stan_awarii WHERE nazwa = 'W trakcie naprawy')
	)
	RETURNING id INTO id_przebieg;
	INSERT INTO awaria_przebieg_specjalista VALUES (
		id_przebieg, _id_uzytkownik
	);
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.awaria_naprawiona | type: FUNCTION --
DROP FUNCTION IF EXISTS public.awaria_naprawiona(integer,integer) CASCADE;
CREATE FUNCTION public.awaria_naprawiona ( _id_uzytkownik integer,  _id integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	id_przebieg integer;
BEGIN

IF NOT EXISTS (SELECT 1 FROM specjalista WHERE id_pracownik = _id_uzytkownik) THEN
	RETURN 'Specjalista nie istnieje';
ELSIF NOT (SELECT dostep_do_przydzielonych_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF NOT EXISTS (SELECT 1 FROM awaria WHERE id = _id) THEN
	RETURN 'Awaria nie istnieje';
ELSE
	UPDATE awaria SET id_stan_awarii = stan_awarii.id
	FROM stan_awarii
	WHERE stan_awarii.nazwa = 'Naprawiono'
	AND awaria.id = _id;
	INSERT INTO awaria_przebieg (
		id_awaria,
		id_stan_awarii
	) VALUES
	(
		_id,
		(SELECT id FROM stan_awarii WHERE nazwa = 'Naprawiono')
	)
	RETURNING id INTO id_przebieg;
	INSERT INTO awaria_przebieg_specjalista VALUES (
		id_przebieg, _id_uzytkownik
	);
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.awaria_zatwierdz | type: FUNCTION --
DROP FUNCTION IF EXISTS public.awaria_zatwierdz(integer,integer) CASCADE;
CREATE FUNCTION public.awaria_zatwierdz ( _id_uzytkownik integer,  _id integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
DECLARE
	spec_id integer;
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik WHERE id = _id_uzytkownik) THEN
	RETURN 'Pracownik nie istnieje';
ELSIF NOT (SELECT dostep_do_swoich_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF NOT EXISTS (SELECT 1 FROM awaria WHERE id = _id) THEN
	RETURN 'Awaria nie istnieje';
ELSE
	DELETE FROM awaria_specjalista WHERE id_awaria = _id
	RETURNING id_pracownik_specjalista INTO spec_id;
	UPDATE awaria SET id_stan_awarii = stan_awarii.id
	FROM stan_awarii
	WHERE stan_awarii.nazwa = 'Zaakceptowano'
	AND awaria.id = _id;
	UPDATE specjalista SET
	ilosc_napraw = ilosc_napraw + 1,
	ilosc_pomyslnych_napraw = ilosc_pomyslnych_napraw + 1
	WHERE id_pracownik = spec_id;
	INSERT INTO awaria_przebieg (
		id_awaria,
		id_stan_awarii
	) VALUES
	(
		_id,
		(SELECT id FROM stan_awarii WHERE nazwa = 'Zaakceptowano')
	);
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.zmien_nazwe_typu_awarii | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zmien_nazwe_typu_awarii(integer,integer,text) CASCADE;
CREATE FUNCTION public.zmien_nazwe_typu_awarii ( _id_uzytkownik integer,  _id integer,  _nazwa text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik
WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF _id IS NOT NULL AND NOT EXISTS (SELECT 1 FROM typ_awarii
WHERE id = _id) THEN
	RETURN 'Typ awarii nie istnieje';
ELSIF _nazwa IN (SELECT nazwa FROM typ_awarii) THEN
	RETURN 'Podana nazwa awarii już istnieje.';
ELSIF length(_nazwa) = 0 THEN
	RETURN 'Nowa nazwa typu awarii nie może być pusta';
ELSE
	UPDATE typ_awarii SET nazwa = _nazwa
	WHERE id = _id;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.zmien_rodzica_typu_awarii | type: FUNCTION --
DROP FUNCTION IF EXISTS public.zmien_rodzica_typu_awarii(integer,integer,integer) CASCADE;
CREATE FUNCTION public.zmien_rodzica_typu_awarii ( _id_uzytkownik integer,  _id integer,  _rodzic integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik
WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF _id IS NOT NULL AND NOT EXISTS (SELECT 1 FROM typ_awarii
WHERE id = _id) THEN
	RETURN 'Typ awarii nie istnieje';
ELSE
	UPDATE typ_awarii SET rodzic = _rodzic
	WHERE id = _id;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.usun_typ_awarii | type: FUNCTION --
DROP FUNCTION IF EXISTS public.usun_typ_awarii(integer,integer) CASCADE;
CREATE FUNCTION public.usun_typ_awarii ( _id_uzytkownik integer,  _id integer)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik
WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF _id IS NOT NULL AND NOT EXISTS (SELECT 1 FROM typ_awarii
WHERE id = _id) THEN
	RETURN 'Typ awarii nie istnieje';
ELSE
	DELETE FROM typ_awarii WHERE id = _id;
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: public.dodaj_typ_awarii | type: FUNCTION --
DROP FUNCTION IF EXISTS public.dodaj_typ_awarii(integer,integer,text) CASCADE;
CREATE FUNCTION public.dodaj_typ_awarii ( _id_uzytkownik integer,  _rodzic integer,  _nazwa text)
	RETURNS text
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	COST 1
	AS $$
BEGIN

IF NOT EXISTS (SELECT 1 FROM pracownik
WHERE id = _id_uzytkownik) THEN
	RETURN 'Uzytkownik nie istnieje';
ELSIF NOT (SELECT dostep_do_awarii FROM typ_uzytkownika
WHERE id = _id_uzytkownik) THEN
	RETURN 'Brak dostepu';
ELSIF _nazwa IN (SELECT nazwa FROM typ_awarii) THEN
	RETURN 'Podana nazwa awarii już istnieje.';
ELSIF length(_nazwa) = 0 THEN
	RETURN 'Nazwa typu awarii nie może być pusta';
ELSIF _rodzic IS NOT NULL AND NOT EXISTS (SELECT 1 FROM typ_awarii
WHERE id = _rodzic) THEN
	RETURN 'Rodzic nie istnieje';
ELSE
	INSERT INTO typ_awarii (
		nazwa,
		rodzic
	) VALUES
	(_nazwa, _rodzic);
	RETURN 'OK';
END IF;

END;
$$;
-- ddl-end --

-- object: typ_awarii_fk | type: CONSTRAINT --
ALTER TABLE public.typ_awarii DROP CONSTRAINT IF EXISTS typ_awarii_fk CASCADE;
ALTER TABLE public.typ_awarii ADD CONSTRAINT typ_awarii_fk FOREIGN KEY (rodzic)
REFERENCES public.typ_awarii (id) MATCH FULL
ON DELETE CASCADE ON UPDATE NO ACTION;
-- ddl-end --


-- Appended SQL commands --
-- widoki --

CREATE VIEW widok_typ_awarii AS
WITH RECURSIVE rodzice(id, nazwa, zagniezdzenie, rodzic, sciezka) AS
(
	SELECT typ_awarii.id, typ_awarii.nazwa, 0, typ_awarii.rodzic, typ_awarii.nazwa
	FROM typ_awarii WHERE typ_awarii.rodzic IS NULL
	UNION ALL
	SELECT typ_awarii.id, typ_awarii.nazwa, zagniezdzenie + 1, typ_awarii.rodzic, sciezka || ' -> ' || typ_awarii.nazwa
	FROM typ_awarii JOIN rodzice ON rodzice.id = typ_awarii.rodzic
)
SELECT id, rodzic, repeat('- ', zagniezdzenie) || nazwa as nazwa, sciezka FROM rodzice
ORDER BY sciezka;

CREATE VIEW widok_pracownik_dane AS
SELECT
	pracownik.id,
	concat(pracownik.imie, ' ', pracownik.nazwisko, '
dział: ', dzial.nazwa, '
', miejsce_pracy.ulica, ' ', miejsce_pracy.nr_domu, '
', miejsce_pracy.kod_pocztowy, ' ', miejsce_pracy.miejscowosc,
		CASE pracownik.telefon IS NOT NULL WHEN TRUE THEN
			concat('
tel.: ', pracownik.telefon)
		END,
		CASE pracownik.email IS NOT NULL WHEN TRUE THEN
			concat('
e-mail: ', pracownik.email)
		END
	) as dane
FROM
	pracownik
	JOIN dzial ON pracownik.id_dzial = dzial.id
	JOIN miejsce_pracy ON dzial.id_miejsce_pracy = miejsce_pracy.id
ORDER BY pracownik.id ASC;

CREATE VIEW widok_awaria AS
SELECT
	awaria.id,
	awaria.temat,
	awaria.opis,
	to_char(awaria.data_zgloszenia, 'DD.MM.YYYY HH24:MI') as data_zgloszenia,
	widok_typ_awarii.sciezka as typ_awarii,
	priorytet_awarii.nazwa as priorytet_awarii,
	stan_awarii.nazwa as stan_awarii,
	prac.id as id_pracownik,
	prac.dane as dane_pracownik,
	spec.id as id_specjalista,
	spec.dane as dane_specjalista
FROM
	awaria
	JOIN priorytet_awarii ON awaria.id_priorytet_awarii = priorytet_awarii.id
	JOIN stan_awarii ON awaria.id_stan_awarii = stan_awarii.id
	JOIN widok_pracownik_dane AS prac ON awaria.id_pracownik = prac.id
	LEFT JOIN widok_typ_awarii ON awaria.id_typ_awarii = widok_typ_awarii.id
	LEFT JOIN awaria_specjalista ON awaria.id = awaria_specjalista.id_awaria
	LEFT JOIN awaria_oczekujaca ON awaria.id = awaria_oczekujaca.id_awaria
	LEFT JOIN specjalista
		ON awaria_specjalista.id_pracownik_specjalista = specjalista.id_pracownik
		OR awaria_oczekujaca.id_pracownik_specjalista = specjalista.id_pracownik
	LEFT JOIN widok_pracownik_dane AS spec ON specjalista.id_pracownik = spec.id
ORDER BY awaria.id ASC;

CREATE VIEW widok_przebieg_awarii AS
SELECT
	awaria_przebieg.id,
	awaria_przebieg.id_awaria,
	stan_awarii.nazwa as stan_awarii,
	to_char(awaria_przebieg.data, 'DD.MM.YYYY HH24:MI') as data,
	widok_pracownik_dane.id as id_specjalista,
	widok_pracownik_dane.dane as dane_specjalista,
	awaria_przebieg_powod.powod,
	awaria_przebieg_powod.sugestie
FROM
	awaria_przebieg
	JOIN stan_awarii ON awaria_przebieg.id_stan_awarii = stan_awarii.id
	LEFT JOIN awaria_przebieg_specjalista ON awaria_przebieg.id = awaria_przebieg_specjalista.id_awaria_przebieg
	LEFT JOIN awaria_przebieg_powod ON awaria_przebieg.id = awaria_przebieg_powod.id_awaria_przebieg
	LEFT JOIN specjalista ON awaria_przebieg_specjalista.id_pracownik_specjalista = specjalista.id_pracownik
	LEFT JOIN widok_pracownik_dane ON specjalista.id_pracownik = widok_pracownik_dane.id
ORDER BY awaria_przebieg.id DESC;

CREATE VIEW widok_pracownik AS
SELECT
	pracownik.id,
	pracownik.imie,
	pracownik.nazwisko,
	pracownik.login,
	pracownik.urlop,
	pracownik.telefon,
	pracownik.email,
	typ_uzytkownika.nazwa as typ,
	dzial.nazwa as dzial,
	miejsce_pracy.ulica,
	miejsce_pracy.nr_domu,
	miejsce_pracy.kod_pocztowy,
	miejsce_pracy.miejscowosc
FROM
	pracownik
	JOIN typ_uzytkownika ON pracownik.id_typ_uzytkownika = typ_uzytkownika.id
	JOIN dzial ON pracownik.id_dzial = dzial.id
	JOIN miejsce_pracy ON dzial.id_miejsce_pracy = miejsce_pracy.id
ORDER BY pracownik.id ASC;

CREATE VIEW widok_specjalista AS
SELECT
	pracownik.id,
	pracownik.urlop,
	widok_pracownik_dane.dane,
	specjalista.id_typ_awarii,
	widok_typ_awarii.sciezka as nazwa_typ_awarii,
	specjalista.ilosc_napraw,
	CASE specjalista.ilosc_napraw
		WHEN 0 THEN null
		ELSE CAST (
		  CAST(specjalista.ilosc_pomyslnych_napraw AS REAL) / CAST(specjalista.ilosc_napraw AS REAL)
		* 100.0 AS TEXT) || '%'
	END as skutecznosc,
	count(coalesce(awaria_specjalista.id_awaria, awaria_oczekujaca.id_awaria)) as obecnie_przydzielono
FROM
	specjalista
	JOIN pracownik ON specjalista.id_pracownik = pracownik.id
	JOIN widok_pracownik_dane ON pracownik.id = widok_pracownik_dane.id
	LEFT JOIN awaria_specjalista ON specjalista.id_pracownik = awaria_specjalista.id_pracownik_specjalista
	LEFT JOIN awaria_oczekujaca ON specjalista.id_pracownik = awaria_oczekujaca.id_pracownik_specjalista
	LEFT JOIN widok_typ_awarii ON specjalista.id_typ_awarii = widok_typ_awarii.id
GROUP BY
	pracownik.id,
	widok_pracownik_dane.dane,
	specjalista.id_typ_awarii,
	widok_typ_awarii.sciezka,
	specjalista.ilosc_napraw,
	specjalista.ilosc_pomyslnych_napraw
ORDER BY pracownik.id ASC;

CREATE VIEW widok_aktywny_specjalista AS
SELECT
	pracownik.id,
	pracownik.imie,
	pracownik.nazwisko
FROM
	specjalista
	JOIN pracownik ON specjalista.id_pracownik = pracownik.id
WHERE
	pracownik.urlop = FALSE
	AND pracownik.id NOT IN (
		SELECT awaria_specjalista.id_pracownik_specjalista
		FROM awaria_specjalista
	)
ORDER BY pracownik.id DESC;

CREATE VIEW widok_dostep AS
SELECT
	pracownik.id,
	pracownik.login,
	pracownik.haslo,
	typ_uzytkownika.dostep_do_awarii,
	typ_uzytkownika.dostep_do_przydzielonych_awarii,
	typ_uzytkownika.dostep_do_specjalistow,
	typ_uzytkownika.dostep_do_swoich_awarii,
	typ_uzytkownika.dostep_do_typow_awarii
FROM
	pracownik
	JOIN typ_uzytkownika ON pracownik.id_typ_uzytkownika = typ_uzytkownika.id
;

-- deletes --

DELETE FROM awaria;
DELETE FROM specjalista;
DELETE FROM pracownik;
DELETE FROM typ_awarii;
DELETE FROM dzial;
DELETE FROM miejsce_pracy;
DELETE FROM typ_uzytkownika;
DELETE FROM stan_awarii;
DELETE FROM priorytet_awarii;

-- priorytet awarii --

INSERT INTO priorytet_awarii
(
	nazwa
)
VALUES
('Niski'),
('Normalny'),
('Wysoki');

-- stan awarii --

INSERT INTO stan_awarii
(
	nazwa
)
VALUES
('Zarzucono'),
('Nie przydzielono'),
('Przydzielono'),
('W trakcie naprawy'),
('Naprawiono'),
('Zaakceptowano');

-- typ uzytkownika --

INSERT INTO typ_uzytkownika
(
	nazwa,
	dostep_do_swoich_awarii,
	dostep_do_awarii,
	dostep_do_przydzielonych_awarii,
	dostep_do_specjalistow,
	dostep_do_typow_awarii
)
VALUES
('Pracownik', TRUE, FALSE, FALSE, FALSE, FALSE),
('Specjalista', TRUE, FALSE, TRUE, FALSE, FALSE),
('Dyspozycjoner', TRUE, TRUE, FALSE, TRUE, TRUE);

-- miejsce pracy --

INSERT INTO miejsce_pracy
(
	ulica,
	nr_domu,
	kod_pocztowy,
	miejscowosc
)
VALUES
('ul. Słoneczna', '3/7', '00-000', 'Warszawa'),
('ul. Pielgrzymów', '45', '23-200', 'Kraków'),
('ul. Zaporowa', '222a', '10-015', 'Łódź');

-- dzialy --

INSERT INTO dzial
(
	nazwa,
	id_miejsce_pracy
)
VALUES
('Public Relations', (SELECT id FROM miejsce_pracy WHERE ulica = 'ul. Słoneczna')),
('Dział kadr', (SELECT id FROM miejsce_pracy WHERE ulica = 'ul. Pielgrzymów')),
('Projektanci', (SELECT id FROM miejsce_pracy WHERE ulica = 'ul. Pielgrzymów')),
('Programiści', (SELECT id FROM miejsce_pracy WHERE ulica = 'ul. Zaporowa')),
('Obsługa techniczna', (SELECT id FROM miejsce_pracy WHERE ulica = 'ul. Zaporowa'));

-- typy awarii --

INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Hardware', NULL);
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Software', NULL);
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Mysz', (SELECT id FROM typ_awarii WHERE nazwa = 'Hardware'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Klawiatura', (SELECT id FROM typ_awarii WHERE nazwa = 'Hardware'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Monitor', (SELECT id FROM typ_awarii WHERE nazwa = 'Hardware'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Jednostka centralna', (SELECT id FROM typ_awarii WHERE nazwa = 'Hardware'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Laptop', (SELECT id FROM typ_awarii WHERE nazwa = 'Hardware'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Eclipse', (SELECT id FROM typ_awarii WHERE nazwa = 'Software'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('PhpStorm', (SELECT id FROM typ_awarii WHERE nazwa = 'Software'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('PgModeler', (SELECT id FROM typ_awarii WHERE nazwa = 'Software'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Przycinanie myszy', (SELECT id FROM typ_awarii WHERE nazwa = 'Mysz'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Problem z włączaniem', (SELECT id FROM typ_awarii WHERE nazwa = 'Jednostka centralna'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Konfiguracja środowiska', (SELECT id FROM typ_awarii WHERE nazwa = 'PhpStorm'));
INSERT INTO typ_awarii (nazwa, rodzic) VALUES ('Walidacja SQL', (SELECT id FROM typ_awarii WHERE nazwa = 'PgModeler'));

-- pracownik --

INSERT INTO pracownik
(
	imie,
	nazwisko,
	login,
	haslo,
	email,
	telefon,
	id_dzial,
	id_typ_uzytkownika,
	urlop
)
VALUES
(
	'Olga',
	'Cudna',
	'olala1234',
	'c257c418fc76aac3a30245bb51b46e23c2a82e07', -- haslo_trudne --
	'olga.cudna@oj2.pl',
	null,
	(SELECT id FROM dzial WHERE nazwa = 'Public Relations'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Pracownik'),
	FALSE
),
(
	'Chantal',
	'Hokej',
	'chakej',
	'8cb2237d0679ca88db6464eac60da96345513964', -- 12345 --
	null,
	'123563421',
	(SELECT id FROM dzial WHERE nazwa = 'Obsługa techniczna'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Specjalista'),
	FALSE
),
(
	'Lothar',
	'Lemann',
	'arurunnig',
	'33fb0a65185c925feb75d0610780f9215623e879', -- 3246_yop --
	'arurunnig-3246@yopmail.com',
	'682165988',
	(SELECT id FROM dzial WHERE nazwa = 'Obsługa techniczna'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Dyspozycjoner'),
	FALSE
),
(
	'Klaudia',
	'Gaul',
	'klaudia',
	'0b9f9eeac5046e96fe9aa3988a9dd6839ead9fd6', -- laudN__why --
	'klaudia11@ygmail.com',
	'159865664',
	(SELECT id FROM dzial WHERE nazwa = 'Dział kadr'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Pracownik'),
	FALSE
),
(
	'Antonio',
	'Postgresas',
	'potamto',
	'f618a924ba96214152cc95aecd9b13260043bef4', -- potamto123 --
	'potamto@ygmail.com',
	'765996328',
	(SELECT id FROM dzial WHERE nazwa = 'Projektanci'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Pracownik'),
	FALSE
),
(
	'Phil',
	'Persson',
	'PePersson',
	'bcd333654d3a512f8d8ba6851e6aeb453be6d8b', -- PePersson --
	'phil.pearsson@yo.email.com',
	null,
	(SELECT id FROM dzial WHERE nazwa = 'Programiści'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Pracownik'),
	FALSE
),
(
	'Daniel',
	'Leupold',
	'DLeupold',
	'f8c6ef22f8c4a004adf9fc5ec02bbbe7d6b80a57', -- leupold --
	'lau.pold@ayay.pl',
	'896325544',
	(SELECT id FROM dzial WHERE nazwa = 'Obsługa techniczna'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Specjalista'),
	FALSE
),
(
	'Jenny',
	'Oertel',
	'techno',
	'843d596f02c4cdc94f3a2945264bec03b5b67a60', -- te12345 --
	'techno@ap.pl',
	'455898887',
	(SELECT id FROM dzial WHERE nazwa = 'Obsługa techniczna'),
	(SELECT id FROM typ_uzytkownika WHERE nazwa = 'Specjalista'),
	FALSE
);

-- specjalnosci --

UPDATE specjalista SET id_typ_awarii = null
  WHERE id_pracownik = (SELECT id FROM pracownik WHERE nazwisko = 'Hokej');

UPDATE specjalista SET id_typ_awarii = typ_awarii.id FROM typ_awarii
  WHERE id_pracownik = (SELECT id FROM pracownik WHERE nazwisko = 'Leupold')
  AND typ_awarii.id = (SELECT id FROM typ_awarii WHERE nazwa = 'PgModeler');

UPDATE specjalista SET id_typ_awarii = typ_awarii.id FROM typ_awarii
  WHERE id_pracownik = (SELECT id FROM pracownik WHERE nazwisko = 'Oertel')
  AND typ_awarii.id = (SELECT id FROM typ_awarii WHERE nazwa = 'Konfiguracja środowiska');

-- awarie --

INSERT INTO awaria
(
	temat,
	opis,
	id_priorytet_awarii,
	id_pracownik,
	id_typ_awarii
)
VALUES
(
	'Myszka nie działa',
	'Co jakiś (ok. 3 min.) myszka się wiesza. Może dlatego, że kabelek jest lekko uszkodzony',
	(SELECT id FROM priorytet_awarii WHERE nazwa = 'Niski'),
	(SELECT id FROM pracownik WHERE nazwisko = 'Cudna'),
	(SELECT id FROM typ_awarii WHERE nazwa = 'Mysz')
),
(
	'Data w linuxie jest nieprawidłowa',
	'Mam tak od kilku tygodni, dopiero jak zrobię aktualizację daty w systemie to się zmienia.',
	(SELECT id FROM priorytet_awarii WHERE nazwa = 'Normalny'),
	(SELECT id FROM pracownik WHERE nazwisko = 'Persson'),
	(SELECT id FROM typ_awarii WHERE nazwa = 'Jednostka centralna')
),
(
	'Mam ciągle blue screen-y',
	'Po godzinie pracy na komputerze pojawia się blue screen i pisze "memory access violation".',
	(SELECT id FROM priorytet_awarii WHERE nazwa = 'Wysoki'),
	(SELECT id FROM pracownik WHERE nazwisko = 'Cudna'),
	(SELECT id FROM typ_awarii WHERE nazwa = 'Software')
),
(
	'Konfiguracja debuggera w PhpStorm',
	'Nie mogę klikność opcji debugguj, próbowałam już wszystkiego.',
	(SELECT id FROM priorytet_awarii WHERE nazwa = 'Wysoki'),
	(SELECT id FROM pracownik WHERE nazwisko = 'Cudna'),
	(SELECT id FROM typ_awarii WHERE nazwa = 'PhpStorm')
),
(
	'Problemy porty usb',
	'Podłączenie niektórych urządzeń pod usb albo wywołuje blue screen, albo pojawia się dymek z wiadomością o awarii',
	(SELECT id FROM priorytet_awarii WHERE nazwa = 'Wysoki'),
	(SELECT id FROM pracownik WHERE nazwisko = 'Cudna'),
	(SELECT id FROM typ_awarii WHERE nazwa = 'Hardware')
);
---
