<?php

/**
 * Funkcja pomocnicza do wykonywania określonej funkcji składowanej w bazie
 * @param $params
 * parametry funkcji, jaka ma się wywołać po stronie bazy
 */
function auto_controller($params){
    global $akcja;
    $wynik = wywolaj_funkcje_skladowana($akcja, $params);
    if (!empty($wynik[0]) && $wynik[0][$akcja] == 'OK')
        $_SESSION['blad'] = "Operacja wykonana pomyślnie.";
    else
        $_SESSION['blad'] = $wynik[0][$akcja];
    przekieruj();
}

// funkcje sterują aplikacją na podstawie akcji i zwracają parametry potrzebne
// do wytworzenia odpowiedniej strony html

/**
 * Obsługa błędu 404 - strona nie znaleziona
 * @return array
 */
function blad_404_kontroler(){
    return array(
        "tytul" => "Błąd 404 - strona nie istnieje!"
    );
}

/**
 * Zbiera informacje dla widoku z formularzem logowania
 * @return array
 */
function logowanie_kontroler(){
    return array(
        "tytul" => "Logowanie"
    );
}

/**
 * Wylogowywanie użytkownika
 */
function wyloguj_kontroler(){
    unset($_SESSION['uzytkownik_id']);
    unset($_SESSION['uzytkownik_haslo']);
    unset($_SESSION['dostep_do_awarii']);
    unset($_SESSION['dostep_do_swoich_awarii']);
    unset($_SESSION['dostep_do_przydzielonych_awarii']);
    unset($_SESSION['dostep_do_typow_awarii']);
    unset($_SESSION['dostep_do_specjalistow']);
    przekieruj("logowanie");
}

/**
 * Akcja wykonania logowania
 */
function logowanie_wykonaj_kontroler(){
    $params = wywolaj_widok("widok_dostep", array(
        "login" => $_POST['login'],
        "haslo" => hash("sha1", $_POST['haslo'])
    ), "login = :login AND haslo = :haslo");
    if (empty($params)){
        $_SESSION['blad'] = "Nieprawidłowe dane logowania.";
    }
    else{
        $_SESSION['uzytkownik_id'] = $params[0]['id'];
        $_SESSION['dostep_do_awarii'] = $params[0]['dostep_do_awarii'];
        $_SESSION['dostep_do_swoich_awarii'] = $params[0]['dostep_do_swoich_awarii'];
        $_SESSION['dostep_do_przydzielonych_awarii'] = $params[0]['dostep_do_przydzielonych_awarii'];
        $_SESSION['dostep_do_typow_awarii'] = $params[0]['dostep_do_typow_awarii'];
        $_SESSION['dostep_do_specjalistow'] = $params[0]['dostep_do_specjalistow'];
    }
    przekieruj();
}

/**
 * Przygotowuje dane pod wyświetlenie wszystkich typów awarii
 * @return array
 */
function typy_awarii_kontroler(){
    if (spradz_dostep("dostep_do_typow_awarii")){
        return [
            "tytul" => "Przeglądaj typy katerogii",
            "typy" => wywolaj_widok("widok_typ_awarii")
        ];
    }
    return [];
}

/**
 * Kontroler do wyświetlania awarii zgłoszonych przez użytkownika
 * @return array
 */
function awarie_kontroler(){
    if (spradz_dostep("dostep_do_swoich_awarii")){
        return [
            "tytul" => "Moje awarie",
            "awarie" => wywolaj_widok("widok_awaria", array(
                "id_pracownik" => $_SESSION['uzytkownik_id']
            ), "id_pracownik = :id_pracownik"),
            "priorytety" => wywolaj_widok("priorytet_awarii"),
            "kategorie" => wywolaj_widok("widok_typ_awarii")
        ];
    }
    return [];
}

/**
 * Kontroler do wyświetlania awarii dla dyspozycjonera
 * @return array
 */
function zarzadzaj_awariami_kontroler(){
    if (spradz_dostep("dostep_do_awarii")){
        return [
            "tytul" => "Zarządzaj awariami",
            "awarie" => wywolaj_widok("widok_awaria"),
            "specjalisci" => wywolaj_widok("widok_aktywny_specjalista")
        ];
    }
    return [];
}

/**
 * Przygotowuje informacje do wyświetlenia przydzielonych awarii do naprawy dla danego specjalisty
 * @return array
 */
function przydzielone_awarie_kontroler(){
    if (spradz_dostep("dostep_do_przydzielonych_awarii")){
        return [
            "tytul" => "Przydzielone awarie do naprawy",
            "awarie" => wywolaj_widok("widok_awaria", [
                "id_specjalista" => $_SESSION['uzytkownik_id']
            ], "id_specjalista = :id_specjalista"),
            "typy" => wywolaj_widok("widok_typ_awarii"),
            "info" => wywolaj_widok("widok_specjalista", [
                "id" => $_SESSION['uzytkownik_id']
            ], "id = :id")[0]
        ];
    }
    return [];
}

/**
 * Kontroler, który wyświetla przegląd wszystkich specjalistów
 * @return array
 */
function specjalisci_kontroler(){
    if (spradz_dostep("dostep_do_specjalistow")){
        return [
            "tytul" => "Przeglądaj specjalistów",
            "specjalisci" => wywolaj_widok("widok_specjalista")
        ];
    }
    return [];
}

/**
 * Kontroler dla informacji zbiorczych o użytkowniku
 * @return array
 */
function informacje_kontroler(){
    $wynik = [
        "tytul" => "Informacje",
        "info" => wywolaj_widok("widok_pracownik", array(
            "id" => $_SESSION['uzytkownik_id']
        ), "id = :id")[0]
    ];
    if ($_SESSION["dostep_do_przydzielonych_awarii"]) {
        $wynik["specjalizacja"] = wywolaj_widok("widok_specjalista", array(
            "id" => $_SESSION['uzytkownik_id']
        ), "id = :id")[0];
        $wynik["typy"] = wywolaj_widok("widok_typ_awarii");
    }
    return $wynik;
}

/**
 * Kontroler przygotowuje informacje dla widoku pod wyświetlenie przebiegu konkretnej awarii
 * @return array
 */
function przebieg_awarii_kontroler(){
    $to_moja_awaria = !empty(wywolaj_widok("widok_awaria", [
        "id_pracownik" => $_SESSION['uzytkownik_id'],
        "id" => $_GET["id"]
    ], "id = :id AND (id_pracownik = :id_pracownik OR id_specjalista = :id_pracownik)"));
    if ($to_moja_awaria || spradz_dostep("dostep_do_awarii")){
        return [
            "tytul" => "Przebieg awarii",
            "przebieg" => wywolaj_widok("widok_przebieg_awarii", [
                "id_awaria" => $_GET["id"]
            ], "id_awaria = :id_awaria"),
            "info" => wywolaj_widok("widok_awaria", [
                "id" => $_GET["id"]
            ], "id = :id")[0]
        ];
    }
    return [];
}

function zmien_telefon_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_POST['telefon']
    ]);
}

function zmien_email_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_POST['email']
    ]);
}

function zmien_haslo_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        sha1($_POST['stare_haslo']),
        sha1($_POST['nowe_haslo'])
    ]);
}

function ustaw_urlop_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id']
    ]);
}

function zdejmij_urlop_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id']
    ]);
}

/**
 * Zmienia preferowany tryb awarii przypisany do specjalisty
 */
function zmien_preferowany_typ_awarii_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        empty($_POST['id']) ? null : $_POST['id']
    ]);
}

function dodaj_awarie_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        empty($_POST['typ']) ? null : $_POST['typ'],
        $_POST['temat'],
        $_POST['opis'],
        $_POST['priorytet']
    ]);
}

/**
 * Przydzielanie awarii dla specjalisty przez dyspozycjonera
 */
function awaria_przydziel_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['awaria_id'],
        $_POST['specjalista_id']
    ]);
}

/**
 * Zmiana stanu awarii na 'Nie przydzielono' tzn. awaria nie została naprawiona i należy ją znowu przydzielić
 */
function awaria_utrzymana_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['id'],
        $_POST['powod'],
        $_POST['sugestie']
    ]);
}

/**
 * Rezygnacja z awarii - brak możliwości wznowienia / przydziału specjalisty
 */
function awaria_zarzuc_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['id'],
        $_POST['powod'],
        $_POST['sugestie']
    ]);
}

/**
 * Przyjmowanie awarii przez specjalistę, który został wytypowany przez dyspozycjonera do rozwiązania problemu
 */
function awaria_przyjmij_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['id']
    ]);
}

/**
 * Oznaczenie awarii jako naprawiona przez specjalistę
 */
function awaria_naprawiona_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['id']
    ]);
}

/**
 * Zatwierdzanie naprawionej awarii przez pracownika - po zatwierdzeniu awaria zostaje zamknięta
 */
function awaria_zatwierdz_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['id']
    ]);
}

function zmien_nazwe_typu_awarii_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        $_GET['id'],
        $_POST['nazwa']
    ]);
}

function zmien_rodzica_typu_awarii_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        empty($_POST['id']) ? null : $_POST['id'],
        empty($_POST['rodzic']) ? null : $_POST['rodzic']
    ]);
}

function usun_typ_awarii_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        empty($_GET['id']) ? null : $_GET['id']
    ]);
}

function dodaj_typ_awarii_kontroler(){
    auto_controller([
        $_SESSION['uzytkownik_id'],
        empty($_POST['rodzic']) ? null : $_POST['rodzic'],
        $_POST['nazwa']
    ]);
}

